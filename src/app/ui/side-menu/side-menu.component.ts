import { MenuItem } from './../models/menu';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ilegra-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  @Input()
  public menuItens: MenuItem[];
  constructor() { }

  ngOnInit() {
  }

}
