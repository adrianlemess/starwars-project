import { UiTestingModule } from './../../testing/modules/ui-testing.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutBaseComponent } from './layout-base.component';
import {  Component } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'ilegra-dummy',
  template: `
    <ilegra-layout-base>
      <h1> Component test </h1>
    </ilegra-layout-base>
  `
})
class DummyComponent {

}

describe('LayoutBaseComponent', () => {
  let component: LayoutBaseComponent;
  let fixture: ComponentFixture<LayoutBaseComponent>;
  let componentDummy: DummyComponent;
  let fixtureDummy: ComponentFixture<DummyComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        UiTestingModule
      ],
      declarations: [
        DummyComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutBaseComponent);
    component = fixture.componentInstance;

    fixtureDummy = TestBed.createComponent(DummyComponent);
    componentDummy = fixtureDummy.componentInstance;
    fixtureDummy.detectChanges();

    fixture.detectChanges();
  });

  it('should create component layout-base', () => {
    expect(component).toBeTruthy();
  });

  it('Should render something instead of ng-content inside of <ilegra-layout-base></ilegra-layout-base>', () => {
    const h1  = fixtureDummy.debugElement.nativeElement.querySelector('h1');
    expect(h1.textContent).toContain('Component test ');
  });

  it('Should render <ilegra-side-menu></ilegra-side-menu>, <ilegra-header></ilegra-header>', () => {
    const ilegraSideMenu = fixtureDummy.debugElement.nativeElement.querySelector('ilegra-side-menu');
    const ilegraHeader = fixtureDummy.debugElement.nativeElement.querySelector('ilegra-header');
    const ilegraNavbar = fixtureDummy.debugElement.nativeElement.querySelector('ilegra-navbar');

    expect(ilegraSideMenu).toBeDefined();
    expect(ilegraHeader).toBeDefined();
    expect(ilegraNavbar).toBeDefined();

  });
});
