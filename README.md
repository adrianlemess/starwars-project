# Projeto Ilegra

Este projeto é destinado a prova técnica realizada pela Ilegra, utilizando a API do [Star Wars](https://swapi.co). 

## Sumário

  - [Cobertura dos testes](#cobertura-dos-testes)
  - [Introdução](#introdu%C3%A7%C3%A3o)
    - [Principais recursos utilizados](#principais-recursos-utilizados)
    - [Pré-requisitos](#pr%C3%A9-requisitos)
    - [Instalação em desenvolvimento sem Docker](#instala%C3%A7%C3%A3o-em-desenvolvimento-sem-docker)
    - [Instalação do Ambiente com Docker](#instala%C3%A7%C3%A3o-do-ambiente-com-docker)
  - [Testes Unitários](#testes-unit%C3%A1rios)
  - [Produção](#produ%C3%A7%C3%A3o)
    - [Uso do GitLab](#uso-do-gitlab)
  - [Considerações](#considera%C3%A7%C3%B5es)
  - [Possíveis melhorias](#poss%C3%ADveis-melhorias)
  - [Autor](#autor)
  - [License](#license)

## Cobertura dos testes

| Branch          | Status                     | Coverage                 |
|-----------------|----------------------------|--------------------------|
| Master          | ![Master][status_master] | ![Master][coverage_master] |

## Introdução

Nessa seção é descrito as dependências utilizadas na aplicação como um todo, instruções de como iniciar o projeto em modo de desenvolvimento, realização de testes e deploy da aplicação.

Disponível em: https://ilegra-projeto.firebaseapp.com

### Principais recursos utilizados

* [Angular](http://angular.io) - O framework web utilizado. Versão 6 utilizada, última até o momento.
* [Node](https://nodejs.org/) - Necessário para o Angular.
* [NPM](https://www.npmjs.com) - Gerenciador de Dependências.
* [Jasmine](https://jasmine.github.io) - Framework de testes, necessário para criação dos specs, contendo spies, stubs, assertions e mais.
* [Karma](https://karma-runner.github.io/2.0/index.html) - Runner responsável por fornecer o ambiente necessário para rodar os specs.
 * [NgRx](https://github.com/ngrx) - Biblioteca semelhante ao redux para implementação de gerenciamento de estado da aplicação baseado na arquitetura Flux.
*  [Bootstrap](https://getbootstrap.com) - Framework de estilo, foi utilizado apenas alguns recursos.
*  [StarWars Glyphicon](http://starwarsglyphicons.com) - Framework de icones baseado no font-awesome, com o tema de star wars.
*  [FontAwesome](http://fontawesome.io) - Framework de icones baseado necessário para rodar os icones do star-wars.
*  [GitLab](http://gitlab.com) - Repositório de versionamento do código e CI com pipelines de Lint, test, build e deploy.
*  [Docker e Docker Compose](https://www.docker.com) - Ferramenta de criação de containers para facilitar o processo de desenvolvimento da aplicação replicando a infra de produção no ambiente de desenvolvimento.
*  [Firebase Hosting](https://firebase.google.com/docs/hosting/?hl=pt-br) - Ferramenta de Cloud fornecida pelo Google para hospedagem e deploy da aplicação.
*  [Star Wars Font](https://fonts2u.com/starwars.font) - Fonte para os Títulos e Subtítulos.

### Pré-requisitos

O que foi necessário para rodar o projeto: 

- Node versão v10.9.0
- GIT instalado
- Conta no Gitlab

"Must Have", não é obrigatório mas facilita o processo de testes e desenvolvimento:

- Docker

### Instalação em desenvolvimento sem Docker

Passos necessários para rodar o projeto sem o docker: 

- Clonar no projeto: 
```
  git clone git@gitlab.com:adrianlemess/starwars-project.git
```
- Instalar o angular-cli para ter acesso as ferramentas necessárias para rodar um projeto Angular em desenvolvimento: 

```
npm install -g @angular/cli
```

- Entrar no diretório do projeto e instalar as dependências necessárias:

```
cd projeto-ilegra

npm install
```

- rodar o projeto:
```
npm start ou ng serve
```

NOTA: O comando npm start configurado nos scripts do package.json, executa o --aot para já evitar possíveis erros na hora de gerar uma build com esse recurso, que otimiza o bundle final.

- Se tudo der certo no terminal, abra no navegador pela URL localhost:4200 que qualquer modificação no projeto refletirá no browser.
  - https://localhost:4200
  

### Instalação do Ambiente com Docker

O processo para rodar o projeto com docker é relativamente mais fácil.

- Instale as dependências do npm no seu hosting local
```
npm install
```
- Se for a primeira vez rodando esse projeto rode o comando abaixo com a flag --build para gerar as imagens:
```
docker-compose -d up --build
```


- Após isso:
```
docker-compose -d up
```

NOTA: A flag -d é para rodar os serviços em modo detached, caso queira ver os logs no console executa sem essa flag.

## Testes Unitários

Para rodar os testes unitários é necessário rodar o comando: 

```
npm run test
```

ou 

```
ng test
```

Caso tenha optado por utilizar o docker, os testes serão executados junto com o ng-serve do projeto.

ex: 
```
docker-compose up
```

Em ambos os casos para abrir o navegador e poder olhar os testes rodando abra a url:
- http://localhost:9876

As funções `fit` e `fdescribe` rodam apenas os especs selecionados. `xit` e `xdescribe` Rodam os testes menos os specs selecionados. 

NOTA: Não commitar com isso =)

## Produção

  Foi configurado um pipeline no GitLab com o objetivo de realizar o deploy da aplicação sempre que for feito um commit ou merge request para a branch master. 
  
  Caso seja necessário efetuar um build de produção fora desse pipeline ou com o objetivo de realizar um deploy em outro hosting, basta criar um pacote dist com o comando:

  ```
  npm run build:prod
  ```

## Uso do GitLab
  - Criação de um board com as etapas Todo, Doing e Closed. [Link do Board](https://gitlab.com/adrianlemess/starwars-project/boards?=)
  - Criação de issues para poder relacionar nos commits.
  [Link das Issues](https://gitlab.com/adrianlemess/starwars-project/issues)
  - Utilização de feature/branch. [Link das Branchs](https://gitlab.com/adrianlemess/starwars-project/branches)
  - Utilização de Merge request para Master. [Link dos Merge Requests](https://gitlab.com/adrianlemess/starwars-project/merge_requests)
  - Pipeline com os stages de Lint, Test e Build. [Link dos Pipelines ](https://gitlab.com/adrianlemess/starwars-project/pipelines)

## Considerações

- Testes do folder shared foram de caixa preta testando direto o uso do component.
- Crédito open-crawling ao link: https://codepen.io/christopherkade/pen/rJVPjz.
- Loading adicionado a cada request.
- Utilização de modelo flux para gerenciamento de estado, facilitando a comunicação entre componentes que não possuem ligação direta. Além de conceber um gerenciamento de estado centralizado, a biblioteca NGRX fornece um observable, facilitando aplicar reatividade a aplicação.
- AfterAll em alguns components serviu para limpar o output no browser pelo Karma para rodar os specs.
- Tela 404 implementada para URLs não mapeadas na aplicação.


## Possíveis melhorias

- Adicionar um search .
- Nenhum tratamento de images - Subir ela pra cloud deixando-as progressivas e não consumindo o tamanho do bundle.
- Criar um CDN para a fonte do Star Wars para melhorar o tamanho do bundle.
- Fazer um toggle pro side menu quando for mobile e deixar o side-menu escondido para melhorar a responsividade.
- Suavizar com animation algumas transições de tela.
- Como a API não tem imagem pros itens não quis criar um card pois achei desnecessário

  
## Autor

* **Adrian Lemes Caetano** -  [GitHub](https://github.com/adrianlemess)

<a href="https://adrianlemess.github.io">
  <img 
  alt="Imagem do Autor Adrian Lemes" src="https://avatars1.githubusercontent.com/u/12432777?s=400&u=927d77dcc0b02c1ac69360f2194336a2517e6f08&v=4" width="100">
</a>

## License

Este projeto possui Licença MIT - veja o arquivo [LICENSE.md](LICENSE.md) para saber mais detalhes.


[coverage_master]:https://gitlab.com/adrianlemess/starwars-project/badges/master/coverage.svg "CoverageMaster"

[status_master]:https://gitlab.com/adrianlemess/starwars-project/badges/master/pipeline.svg "StatusMaster"
